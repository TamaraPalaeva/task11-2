package ru.sberbank.thread;

import ru.sberbank.service.ThreadPool;

import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;


public class FixedThreadPool implements ThreadPool {
    private final int nThreads;
    private final TreadOfPool[] threads;
    private final Queue<Runnable> pool;
    private volatile boolean isRunning = true;

    public FixedThreadPool(int nThreads) {
        this.nThreads = nThreads;
        threads = new TreadOfPool[nThreads];
        pool = new LinkedBlockingQueue();
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new TreadOfPool("Pool-");
        }
    }

    // запускает потоки. Потоки бездействуют до тех пор, пока не появится новое задание в очереди
    public void start() {
        for (TreadOfPool thread : threads) {
            new Thread(thread).start();
        }
    }

    // складывает это задание в очередь. Освободившийся поток должен выполнить это
    @Override
    public void execute(Runnable runnable) {
        synchronized (pool) {
            pool.add(runnable);
            pool.notify();
        }
    }
    public void shutdown() {
        isRunning = false;
    }

    private class TreadOfPool implements Runnable {

        public TreadOfPool(String name) {
        }

        public void run() {
            while (isRunning) {
                  Runnable nextTask = pool.poll();
                  if (nextTask != null){
                      nextTask.run();
                  }
            }
        }
    }


}