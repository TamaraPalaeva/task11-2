package ru.sberbank.thread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

public class MyTread implements Runnable {

    private String name;
    private static int count = 1;

    public MyTread(String name) {
        this.name = name + count;
        count++;
    }

    public void run() {
        try {
            System.out.println("Запущена задача " + name);
            Thread.sleep(1);
            System.out.println("Окончание работы задачи " + name);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

