package ru.sberbank.thread;

import com.sun.corba.se.spi.ior.TaggedProfileTemplate;
import ru.sberbank.service.ThreadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {
    private final List<Thread> threads;
    private final Queue<Runnable> pool;
    private int minThreads;
    private int maxThreads;
    private volatile boolean isRunning = true;


    public ScalableThreadPool(int minThreads, int maxThreads) {
        this.minThreads = minThreads;
        this.maxThreads = maxThreads;
        this.threads = new ArrayList<Thread>();
        pool = new LinkedBlockingQueue();
        for (int i = 0; i < minThreads; i++) {
            threads.add(new Thread(new TreadOfPool()));
        }
    }

    public void start() throws InterruptedException {
        for (Thread thread : threads) {
            thread.start();
        }
        System.out.println("Рабочих потоков = " + threads.size());
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (pool) {
            pool.add(runnable);
            pool.notify();
            if (pool.size() > 0) {
                threadsAdd();
            } else
                if (pool.size() == 0 && threads.size() > minThreads) {
                threadDelete();
            }
        }

    }

    private void threadDelete() {
        threads.remove(threads.size() - 1);
        System.out.println("Сокращение пула. Рабочих потоков = " + threads.size());
    }

    private void threadsAdd() {
        if (threads.size() < maxThreads) {
            Thread thread = new Thread(new TreadOfPool());
            threads.add(thread);
            thread.start();
            System.out.println("Расширение пула. Рабочих потоков = " + threads.size());
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private class TreadOfPool implements Runnable {

        public TreadOfPool() {
        }

        public void run() {
            while (isRunning) {
                Runnable nextTask = pool.poll();
                if (nextTask != null) {
                    nextTask.run();
                }
            }
        }
    }

}