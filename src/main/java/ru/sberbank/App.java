package ru.sberbank;


import ru.sberbank.thread.FixedThreadPool;
import ru.sberbank.thread.MyTread;
import ru.sberbank.thread.ScalableThreadPool;

public class App {
// Максимальное количество потоков в пуле потоков
    static final int FixedPool = 3;
    static final int FixedTask = 10;
    static final int MinPool = 2;
    static final int MaxPool = 5;


    public static void main(String[] args) throws InterruptedException {

//        FixedThreadPool fixedPool = new FixedThreadPool(FixedPool);
//        fixedPool.start();
//        for (int i = 0; i < FixedTask; i++) {
//            MyTread tread = new MyTread("Task-");
//            fixedPool.execute(tread);
//        }
//        Thread.sleep(100);
//        fixedPool.shutdown();

        ScalableThreadPool scalableThreadPool = new ScalableThreadPool(MinPool, MaxPool);
        scalableThreadPool.start();
        for (int i = 0; i < FixedTask; i++) {
            MyTread tread = new MyTread("Task-");
            scalableThreadPool.execute(tread);
        }
        Thread.sleep(10000);
        scalableThreadPool.shutdown();



    }

}
