package ru.sberbank.service;

import java.util.concurrent.Executor;

public interface ThreadPool extends Executor{

    void start() throws InterruptedException;

    public void execute(Runnable runnable);
}
